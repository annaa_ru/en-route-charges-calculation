#The objetive of Labels class is to process the results to assign them to airspace and flight. Then, the results will be plotted and exported.
import csv


class Labels:

    def __init__(self):
        self.country = []
        self.points = []
        self.charge = []
        self.distance = []
        self.aplication = []
        self.myCost = [['Flight', 'Category', 'Country', 'Cost']]

    #set_labels() function defines different parameters in order to classify them orderly, according to country, coordinates and charge of an specific
    #airspace intersection.
    def set_labels(self, enroutepoints, charges, d):
        self.country = []
        self.points = []
        self.charge = []
        self.distance = []
        self.country.append(enroutepoints[0])
        self.points.append(enroutepoints[1])
        self.charge.append('')
        i = 0
        m = 1
        while i < len(enroutepoints):
            if i <= (len(enroutepoints)-4):
                self.country.append(enroutepoints[i])
                self.points.append(enroutepoints[i+2])
                self.charge.append(charges[m])
            else:
                self.country.append(enroutepoints[i])
                self.points.append(enroutepoints[i+2])
                self.charge.append(charges[m])
            i = i + 3
            m = m + 2

        distance2 = []
        j = 0
        sum = 0
        while j < len(d):
            sum = d[j] + sum
            j = j + 1

        n = 1
        distance2.append(d[0])
        self.distance.append(round(((distance2[0]/sum)*100), 2))
        while n < len(d):
            distance2.append(distance2[n-1] + d[n])
            self.distance.append(round((distance2[n]/sum)*100, 2))
            n = n + 1

    #To export the data to CartoDB platform, it is necessary to create a csv file with the previous processed data.
    def create_csv(self, enroutepoints, sheet, flight):
        i = 0
        myData = [['country', 'lat', 'lon', 'wiglet']]
        while i < len(self.charge):
            if self.country[i] == 'LE':
                self.country[i] = 'Spain Continental'
            if self.country[i] == 'GC':
                self.country[i] = 'Spain Canarias'
            if self.country[i] == 'LP':
                self.country[i] = 'Portugal Lisboa'
            if self.country[i] == 'AZ':
                self.country[i] = 'Portugal S M'
            if self.country[i] == 'LF':
                self.country[i] = 'France'
            if self.country[i] == 'LI':
                self.country[i] = 'Italy'
            if self.country[i] == 'LM':
                self.country[i] = 'Malta'
            if self.country[i] == 'EG':
                self.country[i] = 'GBP United Kingdom'
            if self.country[i] == 'EI':
                self.country[i] = 'Ireland'
            if self.country[i] == 'EB':
                self.country[i] = 'Belg.-Luxembourg'
            if self.country[i] == 'ED':
                self.country[i] = 'Germany'
            if self.country[i] == 'EH':
                self.country[i] = 'Netherlands'
            if self.country[i] == 'EK':
                self.country[i] = 'Denmark'
            if self.country[i] == 'EN':
                self.country[i] = 'Norway'
            if self.country[i] == 'ES':
                self.country[i] = 'Sweden'
            if self.country[i] == 'LO':
                self.country[i] = 'Austria'
            if self.country[i] == 'LS':
                self.country[i] = 'Switzerland'
            i = i + 1
        string1 = '{0} - {1}{2}'.format(self.country[0], 'DEPARTURE APT', self.charge[0])
        myData.append([string1, self.points[0].x, self.points[0].y])
        sum = 0
        j = 1
        n = 0

        # Also, the results are defined in order to plot in a Pop-Up Legend in CartoDB Platform for each intersection point, that display the cost per country,
        # the acumulative cost and the % of flight performed for the moment.
        while j < (len(self.country)):
            sum = sum + int(self.charge[j])
            if j >= (len(self.country) - 1):
                string = '{0} - {1}: {2}\u20ac \n \ Cost since departure: {3}\u20ac \n \ \u0025Flight progress: {4}\u0025'.format(self.country[j], 'ARRIVAL APT', self.charge[j], str('{:,}'.format(sum)), self.distance[n])
                string2 = '{0}: {1}\u20ac'.format(self.country[j], self.charge[j])
                myData.append([string, self.points[j].x, self.points[j].y, string2])
                self.myCost.append([flight, sheet, self.country[j], self.charge[j]])
                n = n + 1
            else:
                string = '{0}: {1}\u20ac \n \ Cost since departure: {2}\u20ac \n \ \u0025Flight progress: {3}\u0025'.format(self.country[j], '{:,}'.format(self.charge[j]), str('{:,}'.format(sum)), self.distance[n])
                string2 = '{0}: {1}\u20ac'.format(self.country[j], self.charge[j])
                myData.append([string, self.points[j].x, self.points[j].y, string2])
                self.myCost.append([flight, sheet, self.country[j], self.charge[j]])
                n = n + 1
            j = j + 1
        self.myCost.append([flight, sheet, 'Total', sum])

        #Finally, the final csv file is created to exported to Carto.
        myFile = open('labels_{0}.csv'.format(sheet), 'w')
        with myFile:
            writer = csv.writer(myFile)
            writer.writerows(myData)

        return self.myCost








