#The objetive of Postgresql class is to import different important parameters from Airplane Solutions database.
import psycopg2


class Postgresql(object):
    def __init__(self):
        self.db_param = {'host':'13.95.30.195',
                         'database':'SHOGANAI',
                         'user':'postgres',
                         'password':'S1lv3str301'}

    #get_unit_rates() function call the database to get the monthly unit rates from Eurocontrol. The unit rates are determined
    # according to the airspaces defined per a flight (route)
    def get_unit_rates(self, route):

        listunitrates= []

        try:
            connection = psycopg2.connect(**self.db_param)
            cursor = connection.cursor()

            select_query = """SELECT state_member, unit_rate FROM aps_unit_rates WHERE state_member IN ("""
            routestring = "'"+route[0]+"'"
            for country in route[1:]:
                routestring = routestring + ", '" + country + "'"

            select_query = select_query + routestring + ')'
            cursor.execute(select_query)
            listunitrates = cursor.fetchall()
            connection.commit()

        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)

        finally:
            # closing database connection.
            if connection:
                cursor.close()
                connection.close()
            return listunitrates

    #get_mtom() function call the database to get the mtom according to the airplane tail number which perform a flight.
    def get_mtom(self):

        flight_mtom = 0

        try:
            connection = psycopg2.connect(**self.db_param)
            cursor = connection.cursor()

            select_query = "SELECT mtom_tm FROM aps_aircraft WHERE tail_number = '10222IT'"
            cursor.execute(select_query)
            flight_mtom = cursor.fetchall()
            connection.commit()

        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)

        finally:
            # closing database connection.
            if connection:
                cursor.close()
                connection.close()
            return flight_mtom
