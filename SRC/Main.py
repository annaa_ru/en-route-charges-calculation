from Scenario import Flight, Airspace
from Intersection import Intersection
from EnRouteCharge import EnRoute
from Ddbb import Postgresql
from SetLabels import Labels
import pandas as pd
import cartoframes
from carto.auth import APIKeyAuthClient
import webbrowser
import csv

#Insert the airplane type folder
model = "A320"
#Insert the airline folder
airline = "EWG"
#Insert the flight:
flight = "LEBL_EDDS"

#Connection to CARTO DB API
USERNAME = "annaaru"
USR_BASE_URL = "https://{user}.carto.com/".format(user=USERNAME)
auth_client = APIKeyAuthClient(api_key="f3ed48b92fc3a93e1cb452da2454bf9d8e117b35", base_url=USR_BASE_URL)

BASE_URL = "https://{organization}.carto.com/user/{user}/". \
    format(organization="annaaru",
           user="annaaru")

#Read the Excel Sheets, to diferenciate between Initial and Actual Flight
path = '../Coordinates/{0}/{1}/{2}.xlsx'.format(model, airline, flight)
excel = pd.ExcelFile(path)
length = len(excel.sheet_names)

#Create the classes
Airspace = Airspace()
Intersection = Intersection()
EnRoute = EnRoute()
Labels = Labels()


#Iteration of the different flights (in this case, Initial and Actual Trajectory of the same flight)
i = 0
while i <= (len(excel.sheet_names)-1):
    #Create a LineString from flight coordinates
    sheet = excel.sheet_names[i]
    F = Flight()
    F.get_flight_coordinates(path, sheet)

    #Create a Route to define the countries involved
    F.Route = Airspace.get_countries(F.listFlight)

    #Get the Countries Boundaries Polygon from previous Route
    Airspace.get_polys(F.Route)

    #Computation of intersection points between countries and flight
    Intersection.get_intersection(F.FlightCoord, Airspace.polys, F.Route, F.listFlight)
    Intersection.get_en_route_points(F.listFlight)

    #Connection to APS data base to get the Monthly Adjusted Unit Rates from Eurocontrol website
    Sql = Postgresql()
    Unit_rates = Sql.get_unit_rates(F.Route)
    Mtom = Sql.get_mtom()
    Mtom = round(float(Mtom[0][0]), 1)

    #Get the specific Unit Rates from the countries involved
    Factors = EnRoute.get_unit(Unit_rates, F.Route)

    #Distance Calculation between intersection points
    d = EnRoute.get_distance(Intersection.enRoutePoints, Factors)

    #Final Calculation of EN-ROUTE-CHARGES taking account the formula described by Eurocontrol
    Charges = EnRoute.get_enroute_charge(EnRoute.EnRouteFactors, Mtom)

    #Extraction of the flight and charges information for export to Carto DB
    Data = Labels.set_labels(Intersection.enRoutePoints, Charges, d)
    CsvData = Labels.create_csv(Intersection.enRoutePoints, sheet, flight)

    i = i + 1

#Connection to Carto DB to export all the needed information
ds = pd.read_csv('../SRC/Initial.csv')
df = pd.read_csv('../SRC/Actual.csv')
cc = cartoframes.CartoContext(base_url=USR_BASE_URL,
                              api_key="f3ed48b92fc3a93e1cb452da2454bf9d8e117b35")

cc.write(ds, 'flight_initial', overwrite='True')
cc.write(df, 'flight_actual', overwrite='True')

dl = pd.read_csv('../SRC/labels_Initial.csv')
dt = pd.read_csv('../SRC/labels_Actual.csv')
cc.write(dl, 'charges_initial', overwrite='True')
cc.write(dt, 'charges_actual', overwrite='True')

#Automatic opening of the Carto DB Europe Map
webbrowser.open('https://annaaru.carto.com/builder/ab6706fd-72db-4631-a134-feed1b9de7ca/embed')


#Creation of a Summary_EN-ROUTE-CHARGES. There is specified the cost per country, per initial or actual flight and totals
myFile = open('../En-Route-Charges/{0}/{1}/Summary_Charges_{2}.csv'.format(model, airline, flight), 'w')
with myFile:
    writer = csv.writer(myFile)
    writer.writerows(CsvData)