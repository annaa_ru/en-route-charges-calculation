#The principal objetive of this class is to achieve the entry and exit crossing points for each flown airspace.
import shapely
from shapely.geometry import MultiPoint



class Intersection:

    def __init__(self):
        self.intersectionPoints = []
        self.enRoutePoints = []

    #First, this function create a vector (intersectionPoints) using function intersection from shapely library. This vector contains the intersection
    #points of the flight.
    def get_intersection(self, flight, polys, route, fcoords):
        i = 0
        self.intersectionPoints = []
        self.enRoutePoints = []
        while i <= (len(polys) - 1):
            self.intersectionPoints.append(route[i])
            point = polys[i].intersection(flight)
            if type(point) == shapely.geometry.multipoint.MultiPoint:
                if fcoords[0].x > fcoords[len(fcoords)-1].x:
                    a = point[0]
                    b = point[1]
                    point = shapely.geometry.multipoint.MultiPoint([b, a])
            if i < 1:
                if type(point) == shapely.geometry.multipoint.MultiPoint:
                    n = len(point) - 1
                    self.intersectionPoints.append(point[n])
                else:
                    self.intersectionPoints.append(point)
            elif i >= (len(polys)-1):
                if type(point) == shapely.geometry.multipoint.MultiPoint:
                    self.intersectionPoints.append(point[0])
                else:
                    self.intersectionPoints.append(point)
            else:
                if type(point) == shapely.geometry.multipoint.MultiPoint:
                    self.intersectionPoints.append(point[0])
                    n = len(point) - 1
                    self.intersectionPoints.append(point[n])
                else:
                    self.intersectionPoints.append(point)
            i = i+1

    #Finally, this function adds to the vector intersectionPoints the departure point and arrival point that they are not intersected with any airspace.
    def get_en_route_points(self, flight):
        self.enRoutePoints.append(self.intersectionPoints[0])
        self.enRoutePoints.append(flight[0])
        i = 1
        while i <= (len(self.intersectionPoints) - 1):
            self.enRoutePoints.append(self.intersectionPoints[i])
            i = i+1
        i = (len(flight))
        self.enRoutePoints.append(flight[i-1])




