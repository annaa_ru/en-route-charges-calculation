#CONVERSION OF EXCEL BOUNDARIES FILES TO PKL TO OPTIMIZE THE CODE

import pandas
from sympy.geometry import Point
import pickle
from shapely.geometry import LineString


import matplotlib.pyplot as plt

# SpainContinentalCoordinates = pandas.read_excel('../Coordinates/LE.xlsx')
# SpainIslandCoordinates = pandas.read_excel('../Coordinates/CoordinatesSpainIsland.xlsx')
# PortugalContinentalCoordinates = pandas.read_excel('../Coordinates/LP.xlsx')
# PortugalIslandCoordinates = pandas.read_excel('../Coordinates/AZ.xlsx')
# LFCoordinates = pandas.read_excel('../Coordinates/LF.xlsx')
# LICoordinates = pandas.read_excel('../Coordinates/LI.xlsx')
# # LMCoordinates = pandas.read_excel('../Coordinates/LM.xlsx')
# EGCoordinates = pandas.read_excel('../Coordinates/EG.xlsx')
# # EICoordinates = pandas.read_excel('../Coordinates/EI.xlsx')
# # LSCoordinates = pandas.read_excel('../Coordinates/LS.xlsx')
# # EBCoordinates = pandas.read_excel('../Coordinates/EB.xlsx')
# EDCoordinates = pandas.read_excel('../Coordinates/ED.xlsx')
# EHCoordinates = pandas.read_excel('../Coordinates/EH.xlsx')
# LOCoordinates = pandas.read_excel('../Coordinates/LO.xlsx')
# ESCoordinates = pandas.read_excel('../Coordinates/ES.xlsx')
# ENCoordinates = pandas.read_excel('../Coordinates/EN.xlsx')
# EKCoordinates = pandas.read_excel('../Coordinates/EK.xlsx')


# # listPoints = []
# coordinateslat = []
# coordinateslon = []
# coordinates = []
#
#SPAIN
# listPoints = []
# latSpain = (SpainContinentalCoordinates['y']).tolist()
# lonSpain = (SpainContinentalCoordinates['x']).tolist()
# # # plt.plot(lonSpain, latSpain)
# # # plt.show()
# with open('SpainPoints.pkl', 'wb') as output:
#     for i in range(len(latSpain)):
#         p1 = Point([latSpain[i], lonSpain[i]])
#         listPoints.append(p1)
#         if i == int(len(latSpain)/4):
#             print("25%")
#         if i == int(len(latSpain)/2):
#             print("50%")
#         if i == int(len(latSpain)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)
# #
#
# #CANARIAS
# listPoints = []
# latCanarias = (SpainIslandCoordinates['y']).tolist()
# lonCanarias = (SpainIslandCoordinates['x']).tolist()
# # #plt.plot(lonCanarias, latCanarias)
# # # plt.show()
#
# with open('CanariasPoints.pkl', 'wb') as output:
#     for i in range(len(lonCanarias)):
#         p1 = Point([latCanarias[i], lonCanarias[i]])
#         listPoints.append(p1)
#         if i == int(len(lonCanarias)/ 4):
#             print("25%")
#         if i == int(len(lonCanarias)/2):
#             print("50%")
#         if i == int(len(lonCanarias)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)
#
#
# #PORTUGAL
# listPoints = []
# latPortugal = (PortugalContinentalCoordinates['y']).tolist()
# lonPortugal = (PortugalContinentalCoordinates['x']).tolist()
# # plt.plot(lonPortugal, latPortugal)
# # plt.show()
#
# with open('PortugalPoints.pkl', 'wb') as output:
#     for i in range(len(latPortugal)):
#         p1 = Point([latPortugal[i], lonPortugal[i]])
#         listPoints.append(p1)
#         if i == int(len(latPortugal) / 4):
#             print("25%")
#         if i == int(len(latPortugal)/2):
#             print("50%")
#         if i == int(len(latPortugal)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)
#
# # #SANTA MARIA FIR
# listPoints = []
# latSantaMaria = (PortugalIslandCoordinates['y']).tolist()
# lonSantaMaria = (PortugalIslandCoordinates['x']).tolist()
# # plt.plot(latSantaMaria, lonSantaMaria)
# # plt.show()
# #
# with open('SantaMariaPoints.pkl', 'wb') as output:
#     for i in range(len(latSantaMaria)):
#         p1 = Point([latSantaMaria[i], lonSantaMaria[i]])
#         listPoints.append(p1)
#         if i == int(len(latSantaMaria) / 4):
#             print("25%")
#         if i == int(len(latSantaMaria)/2):
#             print("50%")
#         if i == int(len(latSantaMaria)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)

#FRANCE
# listPoints = []
# latFrance = (LFCoordinates['y']).tolist()
# lonFrance = (LFCoordinates['x']).tolist()
# # plt.plot(lonPortugal, latPortugal)
# # plt.show()
#
# with open('FrancePoints.pkl', 'wb') as output:
#     for i in range(len(latFrance)):
#         p1 = Point([latFrance[i], lonFrance[i]])
#         listPoints.append(p1)
#         if i == int(len(latFrance) / 4):
#             print("25%")
#         if i == int(len(latFrance)/2):
#             print("50%")
#         if i == int(len(latFrance)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)

#ITALY
# listPoints = []
# latItaly = (LICoordinates['y']).tolist()
# lonItaly = (LICoordinates['x']).tolist()
# # plt.plot(lonPortugal, latPortugal)
# # plt.show()
#
# with open('ItalyPoints.pkl', 'wb') as output:
#     for i in range(len(latItaly)):
#         p1 = Point([latItaly[i], lonItaly[i]])
#         listPoints.append(p1)
#         if i == int(len(latItaly) / 4):
#             print("25%")
#         if i == int(len(latItaly)/2):
#             print("50%")
#         if i == int(len(latItaly)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)
#
# #MALTA
# listPoints = []
# latMalta = (LMCoordinates['y']).tolist()
# lonMalta = (LMCoordinates['x']).tolist()
# # plt.plot(lonPortugal, latPortugal)
# # plt.show()
#
# with open('MaltaPoints.pkl', 'wb') as output:
#     for i in range(len(latMalta)):
#         p1 = Point([latMalta[i], lonMalta[i]])
#         listPoints.append(p1)
#         if i == int(len(latMalta) / 4):
#             print("25%")
#         if i == int(len(latMalta)/2):
#             print("50%")
#         if i == int(len(latMalta)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)
#

#UK
# listPoints = []
# latUK = (EGCoordinates['y']).tolist()
# lonUK = (EGCoordinates['x']).tolist()
# # plt.plot(lonPortugal, latPortugal)
# # plt.show()
#
# with open('UKPoints.pkl', 'wb') as output:
#     for i in range(len(latUK)):
#         p1 = Point([latUK[i], lonUK[i]])
#         listPoints.append(p1)
#         if i == int(len(latUK) / 4):
#             print("25%")
#         if i == int(len(latUK)/2):
#             print("50%")
#         if i == int(len(latUK)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)
#
# #IRELAND
# listPoints = []
# latIrland = (EICoordinates['y']).tolist()
# lonIrland = (EICoordinates['x']).tolist()
# # plt.plot(lonPortugal, latPortugal)
# # plt.show()
#
# with open('IrlandPoints.pkl', 'wb') as output:
#     for i in range(len(latIrland)):
#         p1 = Point([latIrland[i], lonIrland[i]])
#         listPoints.append(p1)
#         if i == int(len(latIrland) / 4):
#             print("25%")
#         if i == int(len(latIrland)/2):
#             print("50%")
#         if i == int(len(latIrland)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)
#

# #Switzerland
# listPoints = []
# latSui = (LSCoordinates['y']).tolist()
# lonSui = (LSCoordinates['x']).tolist()
# # plt.plot(lonPortugal, latPortugal)
# # plt.show()
#
# with open('SwitzerlandPoints.pkl', 'wb') as output:
#     for i in range(len(latSui)):
#         p1 = Point([latSui[i], lonSui[i]])
#         listPoints.append(p1)
#         if i == int(len(latSui) / 4):
#             print("25%")
#         if i == int(len(latSui)/2):
#             print("50%")
#         if i == int(len(latSui)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)

# #Belgium
# listPoints = []
# latBel = (EBCoordinates['y']).tolist()
# lonBel = (EBCoordinates['x']).tolist()
# # plt.plot(lonPortugal, latPortugal)
# # plt.show()
#
# with open('BelgiumPoints.pkl', 'wb') as output:
#     for i in range(len(latBel)):
#         p1 = Point([latBel[i], lonBel[i]])
#         listPoints.append(p1)
#         if i == int(len(latBel) / 4):
#             print("25%")
#         if i == int(len(latBel)/2):
#             print("50%")
#         if i == int(len(latBel)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)

#Germany
# listPoints = []
# latGer = (EDCoordinates['y']).tolist()
# lonGer = (EDCoordinates['x']).tolist()
# # plt.plot(lonPortugal, latPortugal)
# # plt.show()
#
# with open('GermanyPoints.pkl', 'wb') as output:
#     for i in range(len(latGer)):
#         p1 = Point([latGer[i], lonGer[i]])
#         listPoints.append(p1)
#         if i == int(len(latGer) / 4):
#             print("25%")
#         if i == int(len(latGer)/2):
#             print("50%")
#         if i == int(len(latGer)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)

# #Netherlands
# listPoints = []
# latNet = (EHCoordinates['y']).tolist()
# lonNet = (EHCoordinates['x']).tolist()
# # plt.plot(lonPortugal, latPortugal)
# # plt.show()
#
# with open('NetherlandsPoints.pkl', 'wb') as output:
#     for i in range(len(latNet)):
#         p1 = Point([latNet[i], lonNet[i]])
#         listPoints.append(p1)
#         if i == int(len(latNet) / 4):
#             print("25%")
#         if i == int(len(latNet)/2):
#             print("50%")
#         if i == int(len(latNet)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)

#Kovenhaven
# listPoints = []
# latKoven = (EKCoordinates['y']).tolist()
# lonKoven = (EKCoordinates['x']).tolist()
# # plt.plot(lonPortugal, latPortugal)
# # plt.show()
#
# with open('KovenhavenPoints.pkl', 'wb') as output:
#     for i in range(len(latKoven)):
#         p1 = Point([latKoven[i], lonKoven[i]])
#         listPoints.append(p1)
#         if i == int(len(latKoven) / 4):
#             print("25%")
#         if i == int(len(latKoven)/2):
#             print("50%")
#         if i == int(len(latKoven)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)


#Norway
# listPoints = []
# latNorway = (ENCoordinates['y']).tolist()
# lonNorway = (ENCoordinates['x']).tolist()
# # plt.plot(lonPortugal, latPortugal)
# # plt.show()
#
# with open('NorwayPoints.pkl', 'wb') as output:
#     for i in range(len(latNorway)):
#         p1 = Point([latNorway[i], lonNorway[i]])
#         listPoints.append(p1)
#         if i == int(len(latNorway) / 4):
#             print("25%")
#         if i == int(len(latNorway)/2):
#             print("50%")
#         if i == int(len(latNorway)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)

#Sweden
# listPoints = []
# latSweden = (ESCoordinates['y']).tolist()
# lonSweden = (ESCoordinates['x']).tolist()
# plt.plot(lonPortugal, latPortugal)
# plt.show()

# with open('SwedenPoints.pkl', 'wb') as output:
#     for i in range(len(latSweden)):
#         p1 = Point([latSweden[i], lonSweden[i]])
#         listPoints.append(p1)
#         if i == int(len(latSweden) / 4):
#             print("25%")
#         if i == int(len(latSweden)/2):
#             print("50%")
#         if i == int(len(latSweden)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)

#Austria
# listPoints = []
# latAustria = (LOCoordinates['y']).tolist()
# lonAustria = (LOCoordinates['x']).tolist()
# # plt.plot(lonPortugal, latPortugal)
# # plt.show()
#
# with open('AustriaPoints.pkl', 'wb') as output:
#     for i in range(len(latAustria)):
#         p1 = Point([latAustria[i], lonAustria[i]])
#         listPoints.append(p1)
#         if i == int(len(latAustria) / 4):
#             print("25%")
#         if i == int(len(latAustria)/2):
#             print("50%")
#         if i == int(len(latAustria)*3/4):
#             print("75%")
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)

# #Coordinates of the flight
# listPoints = []
# data = pandas.read_csv('../Coordinates/GCLP-LEBL.csv', sep=',')
# lat = (data['lat']).tolist()
# lon = (data['lon']).tolist()
# with open('FlightPoints.pkl', 'wb') as output:
#     for i in range(len(lat)):
#         p1 = Point([lat[i], lon[i]])
#         listPoints.append(p1)
#     listPoints = LineString(listPoints)
#     pickle.dump(listPoints, output, pickle.HIGHEST_PROTOCOL)

#
# coordinateslat = [latSpain, latCanarias, latPortugal, latSantaMaria]
# coordinateslon = [lonSpain, lonCanarias, lonPortugal, lonSantaMaria]
# coordinates = [coordinateslat, coordinateslon]
#
# with open('AirspaceLat.pkl', 'wb') as output:
#     coordinateslat.append(coordinateslat)
#     pickle.dump(coordinateslat, output, pickle.HIGHEST_PROTOCOL)
#
# with open('AirspaceLon.pkl', 'wb') as output:
#     coordinateslat.append(coordinateslon)
#     pickle.dump(coordinateslon, output, pickle.HIGHEST_PROTOCOL)