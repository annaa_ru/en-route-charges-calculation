#The most important Class is EnRoute. Here, it is defined the different parameters and functions to calculate
# the final en-route charge per country and per flight.
import math

class EnRoute:
    def __init__(self):
        self.en_route_charges = []
        self.GreatDist = []
        self.EnRouteFactors = []
        self.Country = []
        self.R = 6376
        self.WeightFactor = []
        self.initiallatitude = []
        self.initiallongitude = []
        self.finallatitude = []
        self.finallongitude = []
        self.TotalDistance = []

    #get_unit() function creates a vector (list_rates) that assign each unit rate to an airspace acronym, with the objetive to organize the unit rates.
    def get_unit(self, Unit_rates, Route):
        list_rates = []
        i = 0
        j = 0
        while i <= len(Unit_rates):
            while j <= (len(Route) - 1):
                if Route[j] == Unit_rates[i][0]:
                    list_rates.append(Unit_rates[i][1])
                    i = 0
                    j = j + 1
                else:
                    i = i + 1
            if j >= len(Route):
                i = (len(Unit_rates) + 1)
        return list_rates

    #get_distance calculate the orthodromic distance between the two intersection points
    #for each flown airspace.
    def get_distance(self, enRoutePoints, list_rates):
        i = 0
        d = []
        self.Country = []
        self.initiallatitude = []
        self.initiallongitude = []
        self.finallatitude = []
        self.finallongitude = []
        self.GreatDist = []
        self.EnRouteFactors = []
        while i <= (len(enRoutePoints)-1):
            self.Country.append(enRoutePoints[i])
            self.initiallatitude.append(enRoutePoints[i + 1].x)
            self.initiallongitude.append(enRoutePoints[i + 1].y)
            self.finallatitude.append(enRoutePoints[i + 2].x)
            self.finallongitude.append(enRoutePoints[i + 2].y)
            i = i + 3

        i = 0
        while i < len(self.Country):
            self.GreatDist.append(self.Country[i])
            self.EnRouteFactors.append(self.Country[i])
            self.EnRouteFactors.append(list_rates[i])

            #Conversion to radians
            ilat = math.radians(self.initiallatitude[i])
            ilon = math.radians(self.initiallongitude[i])
            flat = math.radians(self.finallatitude[i])
            flon = math.radians(self.finallongitude[i])

            #Latitude and longitude differences
            dlat = flat - ilat
            dlon = flon - ilon

            #Great Circle Distance Calculation
            a = (math.sin(dlat/2))**2 + math.cos(ilat)*math.cos(flat)*(math.sin(dlon/2))**2
            c = 2*math.asin(min(1, math.sqrt(a)))
            d.append(self.R*c)
            self.EnRouteFactors.append(d[i]/100)
            i = i + 1
        return d

    #Using the unit rates defined previously, mtom and Great Circle Distance, the formula to compute the en-route charge is applied.
    #It return a vector Enroute_charge with the results.
    def get_enroute_charge(self, factors, Mtom):
        i = 0
        j = 0
        Enroute_charge = []
        while i < len(factors):
            Enroute_charge.append(factors[i])
            Unit_rate = float(factors[i+1]/100)
            DistanceFactor = factors[i+2]
            Charge = Unit_rate*DistanceFactor*math.sqrt(Mtom/50)
            Enroute_charge.append(round(Charge, 2))
            i = i + 3
        return Enroute_charge



