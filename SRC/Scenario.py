#Scenario function contains the two principals Classes of this script; the objetive is to compute the environment working, airspace scenario.
import pickle
import pandas
import csv
from shapely.geometry import Point
from shapely.geometry import LineString
from shapely.geometry import Polygon

#It is defined the Class Airspace to read the .pkl airspace files and then, convert in Polygons.
class Airspace:
    def __init__(self):

        self.polys = []

        with open('SpainPoints.pkl', 'rb') as input:
            self.SectorSpain = pickle.load(input)
        self.polySpain = Polygon(self.SectorSpain)

        with open('PortugalPoints.pkl', 'rb') as input:
            self.SectorPortugal = pickle.load(input)
        self.polyPortugal = Polygon(self.SectorPortugal)

        with open('CanariasPoints.pkl', 'rb') as input:
            self.SectorCanarias = pickle.load(input)
        self.polyCanarias = Polygon(self.SectorCanarias)

        with open('SantaMariaPoints.pkl', 'rb') as input:
            self.SectorSantaMaria = pickle.load(input)
        self.polySantaMaria = Polygon(self.SectorSantaMaria)

        with open('FrancePoints.pkl', 'rb') as input:
            self.SectorFrance = pickle.load(input)
        self.polyFrance = Polygon(self.SectorFrance)

        with open('ItalyPoints.pkl', 'rb') as input:
            self.SectorItaly = pickle.load(input)
        self.polyItaly = Polygon(self.SectorItaly)

        with open('MaltaPoints.pkl', 'rb') as input:
            self.SectorMalta = pickle.load(input)
        self.polyMalta = Polygon(self.SectorMalta)

        with open('UKPoints.pkl', 'rb') as input:
            self.SectorUK = pickle.load(input)
        self.polyUK = Polygon(self.SectorUK)

        with open('IrlandPoints.pkl', 'rb') as input:
            self.SectorIrland = pickle.load(input)
        self.polyIrland = Polygon(self.SectorIrland)

        with open('BelgiumPoints.pkl', 'rb') as input:
            self.SectorBelgium = pickle.load(input)
        self.polyBelgium = Polygon(self.SectorBelgium)

        with open('GermanyPoints.pkl', 'rb') as input:
            self.SectorGermany = pickle.load(input)
        self.polyGermany = Polygon(self.SectorGermany)

        with open('NetherlandsPoints.pkl', 'rb') as input:
            self.SectorNetherlands = pickle.load(input)
        self.polyNetherlands = Polygon(self.SectorNetherlands)

        with open('AustriaPoints.pkl', 'rb') as input:
            self.SectorAustria = pickle.load(input)
        self.polyAustria = Polygon(self.SectorAustria)

        with open('KovenhavenPoints.pkl', 'rb') as input:
            self.SectorKovenhaven = pickle.load(input)
        self.polyKovenhaven = Polygon(self.SectorKovenhaven)

        with open('NorwayPoints.pkl', 'rb') as input:
            self.SectorNorway = pickle.load(input)
        self.polyNorway = Polygon(self.SectorNorway)

        with open('SwedenPoints.pkl', 'rb') as input:
            self.SectorSweden = pickle.load(input)
        self.polySweden = Polygon(self.SectorSweden)

        with open('SwitzerlandPoints.pkl', 'rb') as input:
            self.SectorSwitzerland = pickle.load(input)
        self.polySwitzerland = Polygon(self.SectorSwitzerland)

    #Using get_countries() function and taking a flight as the input, the output is the different airspaces that this flight crosses (route)
    def get_countries(self, flight):
        Route = []
        for point in flight:
            if self.polySpain.contains(point):
                Route.append('LE')
            if self.polyPortugal.contains(point):
                Route.append('LP')
            if self.polyCanarias.contains(point):
                Route.append('GC')
            if self.polySantaMaria.contains(point):
                Route.append('AZ')
            if self.polyFrance.contains(point):
                Route.append('LF')
            if self.polyItaly.contains(point):
                Route.append('LI')
            if self.polyMalta.contains(point):
                Route.append('LM')
            if self.polyUK.contains(point):
                Route.append('EG')
            if self.polyIrland.contains(point):
                Route.append('EI')
            if self.polyBelgium.contains(point):
                Route.append('EB')
            if self.polyGermany.contains(point):
                Route.append('ED')
            if self.polyNetherlands.contains(point):
                Route.append('EH')
            if self.polyKovenhaven.contains(point):
                Route.append('EK')
            if self.polyNorway.contains(point):
                Route.append('EN')
            if self.polySweden.contains(point):
                Route.append('ES')
            if self.polyAustria.contains(point):
                Route.append('LO')
            if self.polySwitzerland.contains(point):
                Route.append('LS')

        i = 0
        while i <= (len(Route)-1):
            j = i + 1
            while j <= (len(Route)-1):
                if Route[i] == Route[j]:
                    del(Route[j])
                    j = j - 1
                j = j + 1
            i = i + 1
        return Route

    # Using get_polys() function and taking the route previously computed as a input, the output is a vector that defines the airspaces acronyms.
    def get_polys(self, route):
        self.polys = []
        for string in route:
            if string == 'LE':
                self.polys.append(self.SectorSpain)
            if string == 'GC':
                self.polys.append(self.SectorCanarias)
            if string == 'LP':
                self.polys.append(self.SectorPortugal)
            if string == 'AZ':
                self.polys.append(self.SectorSantaMaria)
            if string == 'LF':
                self.polys.append(self.SectorFrance)
            if string == 'LI':
                self.polys.append(self.SectorItaly)
            if string == 'LM':
                self.polys.append(self.SectorMalta)
            if string == 'EG':
                self.polys.append(self.SectorUK)
            if string == 'EI':
                self.polys.append(self.SectorIrland)
            if string == 'EB':
                self.polys.append(self.SectorBelgium)
            if string == 'ED':
                self.polys.append(self.SectorGermany)
            if string == 'EH':
                self.polys.append(self.SectorNetherlands)
            if string == 'EK':
                self.polys.append(self.SectorKovenhaven)
            if string == 'EN':
                self.polys.append(self.SectorNorway)
            if string == 'ES':
                self.polys.append(self.SectorSweden)
            if string == 'LO':
                self.polys.append(self.SectorAustria)
            if string == 'LS':
                self.polys.append(self.SectorSwitzerland)


#This class take the flight Excel file to compute the latitude and longitude of each flight point. The output is a Linestring
# to use this vector in future calculations efficiently.
class Flight:
    def __init__(self):
        self.listFlight = []
        self.FlightCoord = []
        self.Route = []

    def get_flight_coordinates(self, n, sheet):
        data = pandas.read_excel(n, sheetname=sheet)
        lat = (data['Latitude']).tolist()
        lon = (data['Longitude']).tolist()
        Data = [['lat', 'lon']]
        grad = []
        minut = []
        secon = []
        grad2 = []
        minut2 = []
        secon2 = []
        latitude = []
        longitude = []
        for i in range(len(lat)):
            grad.append(lat[i]//10000)
            minut.append((lat[i] % 10000)//100)
            secon.append(lat[i] % 100)
            latitude.append(grad[i] + minut[i]/60 + secon[i]/3600)
            if lon[i]<0:
                grad2.append(abs(lon[i]) // 10000)
                minut2.append((abs(lon[i]) % 10000) // 100)
                secon2.append(abs(lon[i]) % 100)
                longitude.append(-grad2[i] - minut2[i]/60 - secon2[i]/3600)
            else:
                grad2.append(lon[i] // 10000)
                minut2.append((lon[i] % 10000) // 100)
                secon2.append(lon[i] % 100)
                longitude.append(grad2[i] + minut2[i] / 60 + secon2[i] / 3600)
            Data.append([latitude[i], longitude[i]])
            p1 = Point([latitude[i], longitude[i]])
            self.listFlight.append(p1)
        self.FlightCoord = LineString(self.listFlight)

        #The flight coordinates are exported as csv to send to CartoDB Platform.
        myFile = open('{0}.csv'.format(sheet), 'w')
        with myFile:
            writer = csv.writer(myFile)
            writer.writerows(Data)





